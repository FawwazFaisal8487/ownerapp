/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.deviceowner;

import android.app.Activity;
import android.app.KeyguardManager;
import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.UserHandle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Handles events related to device owner.
 */
public class DeviceOwnerReceiver extends DeviceAdminReceiver {

    /**
     * Called on the new profile when device owner provisioning has completed. Device owner
     * provisioning is the process of setting up the device so that its main profile is managed by
     * the mobile device management (MDM) application set up as the device owner.
     */
    @Override
    public void onProfileProvisioningComplete(Context context, Intent intent) {
        // Enable the profile
        DevicePolicyManager manager =
                (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        ComponentName componentName = getComponentName(context);
        manager.setProfileName(componentName, context.getString(R.string.profile_name));
        // Open the main screen
        Intent launch = new Intent(context, MainActivity.class);
        launch.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(launch);
        Log.e("onProfileProvisioning",intent.getAction());
    }

    /**
     * @return A newly instantiated {@link android.content.ComponentName} for this
     * DeviceAdminReceiver.
     */
    public static ComponentName getComponentName(Context context) {
        return new ComponentName(context.getApplicationContext(), DeviceOwnerReceiver.class);
    }

    @Override
    public void onEnabled(@NonNull Context context, @NonNull Intent intent) {
        Toast.makeText(context,"onEnabled",Toast.LENGTH_SHORT).show();
        Log.e("onProfileProvisioning",intent.getAction());
    }

    @Override
    public void onDisabled(@NonNull Context context, @NonNull Intent intent) {
        Toast.makeText(context,"onDisabled",Toast.LENGTH_SHORT).show();
        Log.e("onProfileProvisioning",intent.getAction());
    }

    @Nullable
    @Override
    public CharSequence onDisableRequested(@NonNull Context context, @NonNull Intent intent) {
        DevicePolicyManager dpm = (DevicePolicyManager)context.getApplicationContext().getSystemService(Context.DEVICE_POLICY_SERVICE);
        dpm.lockNow();
        return super.onDisableRequested(context, intent);
    }



    @Override
    public void onPasswordChanged(@NonNull Context context, @NonNull Intent intent, @NonNull UserHandle user) {
        Toast.makeText(context, "onPasswordChanged", Toast.LENGTH_LONG).show();
        Log.e("onProfileProvisioning",intent.getAction());
    }

    @Override
    public void onPasswordSucceeded(@NonNull Context context, @NonNull Intent intent, @NonNull UserHandle user) {
        Toast.makeText(context, "onPasswordSucceeded", Toast.LENGTH_LONG).show();
        Log.e("onProfileProvisioning",intent.getAction());
    }

    @Override
    public void onPasswordSucceeded(@NonNull Context context, @NonNull Intent intent) {
        Toast.makeText(context, "onPasswordSucceeded", Toast.LENGTH_LONG).show();
        Log.e("onProfileProvisioning",intent.getAction());
    }

    @Override
    public void onPasswordFailed(@NonNull Context context, @NonNull Intent intent, @NonNull UserHandle user) {
        Toast.makeText(context, "onPasswordFailed", Toast.LENGTH_LONG).show();
        Log.e("onProfileProvisioning",intent.getAction());
    }

    @Override
    public void onUserStarted(@NonNull Context context, @NonNull Intent intent, @NonNull UserHandle startedUser) {
        Toast.makeText(context, "onUserStarted", Toast.LENGTH_LONG).show();
        Log.e("onProfileProvisioning",intent.getAction());
        super.onUserStarted(context, intent, startedUser);
    }

    @Override
    public void onReceive(@NonNull Context context, @NonNull Intent intent) {
        super.onReceive(context, intent);
        if(intent.getAction().equals(ACTION_DEVICE_ADMIN_ENABLED)){
            Intent openAppIntent = new Intent(context,MainActivity.class);
            openAppIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(openAppIntent);
        }
        if(intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)){
            Intent openAppIntent = new Intent(context,MainActivity.class);
            openAppIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            openAppIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            openAppIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(openAppIntent);
        }
        Log.e("onReceive",intent.getAction());
    }
}
