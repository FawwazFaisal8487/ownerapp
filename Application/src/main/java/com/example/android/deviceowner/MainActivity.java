/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.deviceowner;

import android.app.ActivityManager;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    static boolean lockTask = true;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkAdminPrivilege();
    }

    private void checkAdminPrivilege() {
        DevicePolicyManager manager =
                (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        ComponentName componentName = new ComponentName(this,DeviceOwnerReceiver.class);
        if(manager.isAdminActive(componentName)){
            Toast.makeText(this, "Enabled", Toast.LENGTH_SHORT).show();
//            showFragment(DeviceOwnerFragment.newInstance());
        }else{
            Toast.makeText(this, "Disabled", Toast.LENGTH_SHORT).show();
//            showFragment(InstructionFragment.newInstance());
        }
        if (manager.isDeviceOwnerApp(componentName.getPackageName())) {
            // This app is set up as the device owner. Show the main features.
//            Log.d(TAG, "The app is the device owner.");
//            manager.clearDeviceOwnerApp(componentName.getPackageName());
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                manager.clearProfileOwner(componentName);
//                return;
//            }
            try {
                Runtime.getRuntime().exec("dpm set-device-owner com.example.android.deviceowner/.DeviceOwnerReceiver");
            } catch (IOException e) {
                e.printStackTrace();
            }
            manager.setGlobalSetting(componentName, Settings.Global.AUTO_TIME,"1");
            manager.setGlobalSetting(componentName, Settings.Global.AUTO_TIME_ZONE,"1");
//            manager.setpass
//            manager.resetPassword("1235",DevicePolicyManager.RESET_PASSWORD_REQUIRE_ENTRY);
        } else {
            // This app is not set up as the device owner. Show instructions.
            Log.d(TAG, "The app is not the device owner.");
        }
    }

    private void showFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    @Override
    public void onBackPressed() {
    }
}
